package com.splitwise.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "expense")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExpenseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private double amount;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @ManyToMany
    @JoinTable(name = "participants_expense", joinColumns = @JoinColumn(name = "expense_id"),
    inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<UserEntity> userEntityList;

    @OneToOne(mappedBy = "expense")
    private GroupEntity group;

}
