package com.splitwise.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "grp")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany
    @JoinTable(name = "grp_participants", joinColumns = @JoinColumn(name = "grp_id"),
            inverseJoinColumns = @JoinColumn(name = "participants_id"))
    private List<UserEntity> usergroupEntityList;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private UserEntity userEntity;

    @OneToOne
    @JoinColumn(name = "expense_id")
    private ExpenseEntity expense;
}
