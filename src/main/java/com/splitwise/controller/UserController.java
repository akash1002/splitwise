package com.splitwise.controller;

import com.splitwise.request.AssignExpense;
import com.splitwise.request.CreateExpense;
import com.splitwise.request.CreateGroup;
import com.splitwise.request.CreateUser;
import com.splitwise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/createuser")
    String createUser(@RequestBody CreateUser createUser){
        userService.createUser(createUser);
        return "Data Saved";
    }

    @PostMapping("/creategroup")
    String createGroup(@RequestBody CreateGroup createGroup){
        userService.createGroup(createGroup);
        return "Data Saved";
    }

    @PostMapping("/addparticipants")
    String addParticipants(@RequestBody CreateGroup createGroup){
        userService.addParticipants(createGroup);
        return "Data Saved";
    }

    @PostMapping("/createexpense")
    String createExpense(@RequestBody CreateExpense createExpense){
        userService.createExpense(createExpense);
        return "Data Saved";
    }

    @PostMapping("/addparticipantsexpense")
    String addParticipantsExpense(@RequestBody CreateExpense createExpense){
        userService.addExpenseParticipants(createExpense);
        return "Data Saved";
    }

    @PostMapping("/assignexpense")
    String assignExpenseToGroup(@RequestBody AssignExpense assignExpense){
        userService.assignExpense(assignExpense);
        return "Data Saved";
    }
    @GetMapping("/splittamount/{split}")
    double splitamount(@PathVariable Long expenseId){
        double split = userService.divideExpense(expenseId);
        return split;
    }
}
