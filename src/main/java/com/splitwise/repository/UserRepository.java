package com.splitwise.repository;

import com.splitwise.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity,Long> {
//    @Query(value = "select * from user where email = :email",nativeQuery = true)
    Optional<UserEntity> findByEmail(String email);
}
