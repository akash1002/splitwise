package com.splitwise.service.ServiceImpl;

import com.splitwise.entity.ExpenseEntity;
import com.splitwise.entity.GroupEntity;
import com.splitwise.entity.UserEntity;
import com.splitwise.repository.ExpenseRepository;
import com.splitwise.repository.GroupRepository;
import com.splitwise.repository.UserRepository;
import com.splitwise.request.AssignExpense;
import com.splitwise.request.CreateExpense;
import com.splitwise.request.CreateGroup;
import com.splitwise.request.CreateUser;
import com.splitwise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    ExpenseRepository expenseRepository;
    @Override
    public String createUser(CreateUser createUser) {
        UserEntity userEntity = UserEntity.builder()
                .email(createUser.getEmail())
                .name(createUser.getName())
                .phone(createUser.getPhone())
                .build();
        userRepository.save(userEntity);
        return "User Created";
    }

    @Override
    public String createGroup(CreateGroup createGroup) {
        UserEntity userEntity = convertToEntity(createGroup.getCreated_By());
        List<UserEntity> participants = createGroup.getParticipantsId().stream().map(this::convertToEntity).collect(Collectors.toList());
        GroupEntity group = GroupEntity.builder()
                .name(createGroup.getName())
                .userEntity(userEntity)
                .usergroupEntityList(participants)
                .build();
        groupRepository.save(group);
        return "Group Created";
    }

    @Override
    public String addParticipants(CreateGroup createGroup) {
        GroupEntity group = groupRepository.findById(createGroup.getGroupId()).orElseThrow(null);
        createGroup.getParticipantsId().addAll(group.getUsergroupEntityList().stream().map(UserEntity::getId).collect(Collectors.toList()));
        List<UserEntity> participants = createGroup.getParticipantsId().stream().map(this::convertToEntity).collect(Collectors.toList());
        group.setUsergroupEntityList(participants);
        groupRepository.save(group);
        return "Participants add in existing groups";
    }

    public UserEntity convertToEntity(Long id){
      return userRepository.findById(id).orElseThrow(null);

    }
    @Override
    public String createExpense(CreateExpense createExpense) {
        UserEntity userEntity = convertToEntity(createExpense.getCreated_By());
        List<UserEntity> participants = createExpense.getParticipantsId().stream().map(this::convertToEntity).collect(Collectors.toList());
        ExpenseEntity expense = ExpenseEntity.builder()
                .userEntity(userEntity)
                .amount(createExpense.getAmount())
                .name(createExpense.getName())
                .userEntityList(participants)
                .build();
        expenseRepository.save(expense);
        return null;
    }

    @Override
    public String addExpenseParticipants(CreateExpense createExpense) {
        ExpenseEntity expense = expenseRepository.findById(createExpense.getExpenseId()).orElseThrow(null);
        createExpense.getParticipantsId().addAll(expense.getUserEntityList().stream().map(UserEntity::getId).collect(Collectors.toList()));
        List<UserEntity> participants = createExpense.getParticipantsId().stream().map(this::convertToEntity).collect(Collectors.toList());
        expense.setUserEntityList(participants);
        expenseRepository.save(expense);
        return null;
    }

    @Override
    public String assignExpense(AssignExpense assignExpense) {
        ExpenseEntity expense = expenseRepository.findById(assignExpense.getExpenseId()).orElseThrow(null);
        GroupEntity group = groupRepository.findById(assignExpense.getGroupId()).orElseThrow(null);
       group.setExpense(expense);
       groupRepository.save(group);
        return null;
    }

    @Override
    public double divideExpense(Long expenseId) {
        ExpenseEntity expense = expenseRepository.findById(expenseId).orElseThrow(null);
        if(expense.getGroup() != null){
            GroupEntity groupEntity = expense.getGroup();
           return expense.getAmount()/groupEntity.getUsergroupEntityList().size();
        }else {
            List<UserEntity> userEntity = expense.getUserEntityList();
            return expense.getAmount()/userEntity.size();
        }
    }

}
