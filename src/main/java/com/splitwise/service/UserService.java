package com.splitwise.service;

import com.splitwise.request.AssignExpense;
import com.splitwise.request.CreateExpense;
import com.splitwise.request.CreateGroup;
import com.splitwise.request.CreateUser;

public interface UserService {
    String createUser(CreateUser createUser);
    String createGroup(CreateGroup createGroup);

    String addParticipants(CreateGroup createGroup);

    String createExpense(CreateExpense createExpense);

    String addExpenseParticipants(CreateExpense createExpense);

    String assignExpense(AssignExpense assignExpense);

    double divideExpense(Long expenseId);

}
